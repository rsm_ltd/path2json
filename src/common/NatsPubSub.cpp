#include <cstddef>
#include <cstdlib>
#include <functional>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

#include <nats/nats.h>

#include <NatsPubSub.h>

NatsPubSub::NatsPubSub(const char* servers[], int serversCount, const std::string& caCert, const std::string& certChain,
    const std::string& privateKey, const std::string& user, const std::string& password)
{
    // Create a secure NATS connection
    opts = NULL;
    natsOptions_Create(&opts);

    natsOptions_SetServers(opts, servers, serversCount);

    natsOptions_SetUserInfo(opts, user.c_str(), password.c_str());

    natsOptions_SetSecure(opts, true);
    natsOptions_SkipServerVerification(opts, false); // Cert is self-signed and we provide the root CA

    auto ns = natsOptions_LoadCATrustedCertificates(opts, caCert.c_str());
    if (ns != NATS_OK)
    {
        std::cerr << "ERROR: " << natsStatus_GetText(ns) << std::endl;
    }

    ns = natsOptions_LoadCertificatesChain(opts, certChain.c_str(), privateKey.c_str());
    if (ns != NATS_OK)
    {
        std::cerr << "ERROR: " << natsStatus_GetText(ns) << std::endl;
    }

    natsOptions_SetAllowReconnect(opts, true);
    natsOptions_SetMaxReconnect(opts, 1000000);
    natsOptions_SetReconnectWait(opts, 1000);

    static auto errorHandler = [](natsConnection* nc, natsSubscription* sub, natsStatus err, void* closure) {
        std::cerr << "ERROR: NATS async error: " << err << " " << natsStatus_GetText(err) << std::endl;
    };
    natsOptions_SetErrorHandler(opts, errorHandler, NULL);

    ns = natsConnection_Connect(&conn, opts);
    if (ns != NATS_OK)
    {
        std::cerr << "ERROR: " << natsStatus_GetText(ns) << std::endl;
    }
    else
    {
        std::cerr << "INFO: "
                  << "NATS Connected" << std::endl;
        std::cerr << "INFO: Max payload allowed = " << natsConnection_GetMaxPayload(conn) / 1024 << "KB" << std::endl;
    }
}

void NatsPubSub::publish(std::string subject, std::vector<uint8_t> buf)
{
    // Publish to NATS
    natsStatus s = natsConnection_Publish(conn, subject.c_str(), buf.data(), buf.size());
    if (s != NATS_OK)
    {
        std::cerr << "ERROR: " << natsStatus_GetText(s) << std::endl;
    }
}

void NatsPubSub::subscribe(std::string subject, std::function<void(std::string, std::vector<uint8_t>)> callBack)
{
    static auto onMsg = [](natsConnection* nc, natsSubscription* sub, natsMsg* msg, void* _this) {
        const char* data = natsMsg_GetData(msg);
        uint32_t length = natsMsg_GetDataLength(msg);
        std::vector<uint8_t> dataVec(data, data + length);
        ((NatsPubSub*)_this)->callBack(natsMsg_GetSubject(msg), dataVec);
        natsMsg_Destroy(msg);
    };

    this->callBack = callBack;
    // Creates an asynchronous subscription on subject.
    // When a message is received, the callback onMsg() will be invoked by the client library.
    // A closure can be passed as the last argument.
    natsStatus s = natsConnection_Subscribe(&sub, conn, subject.c_str(), onMsg, this);
    if (s != NATS_OK)
    {
        std::cerr << "ERROR: " << natsStatus_GetText(s) << std::endl;
    }
}

NatsPubSub::~NatsPubSub()
{
    std::cout << "INFO: NatsPubSub::~NatsPubSub " << std::endl;

    natsConnection_Close(conn);
    // Anything that is created needs to be destroyed
    natsConnection_Destroy(conn);
    natsOptions_Destroy(opts);
}

//
// For testing build with:
//
//     $ g++ -std=c++11 -DTEST -o test ../src/common/NatsPubSub.cpp -I../include/ -lnats
//
// Run as:
//
//     $./test
//
#ifdef TEST

void onMsg(std::string subject, std::vector<uint8_t> buff)
{
    std::cout << "Got:     " << subject << ", Data = " << buff.data() << std::endl;
};

int main(int argc, char* argv[])
{
    const char* servers[] = { "nats://localhost:4222" };
    int serversCount = sizeof(servers) / sizeof(servers[0]);
    std::string subject = "test.5yeLABoxYt0o";
    std::string cacert = "../certs/ca/ca-pub-cert.pem";
    std::string certChain = "../certs/client/client-cert-chain.pem";
    std::string privateKey = "../certs/client/client-priv-key.pem";
    std::string user = "test";
    std::string password = "test";
    NatsPubSub pubsub(servers, serversCount, cacert, certChain, privateKey, user, password);

    pubsub.subscribe(subject, onMsg);

    while (1)
    {
        char msg[] = "Hello world!";
        std::vector<uint8_t> msgVec(msg, msg + sizeof(msg));
        std::cout << "Publish: " << subject << ", Data = " << msgVec.data() << std::endl;
        pubsub.publish(subject, msgVec);
        nats_Sleep(1000);
    }
    return (0);
}

#endif
