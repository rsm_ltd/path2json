#include "JasonWriter.h"

JsonWriter::JsonWriter(std::ostream& os)
{
    this->os = &os;
    elementCounts.push(0);
}

void JsonWriter::indent()
{
    for (int l = 1; l < elementCounts.size(); l++)
    {
        *os << "    ";
    }
}

void JsonWriter::insertComma()
{
    if (elementCounts.top() != 0)
    {
        *os << "," << std::endl;
    }
    indent();
}

void JsonWriter::newLine()
{
    *os << std::endl;
}

void JsonWriter::addName(std::string name)
{
    *os << "\"" << name << "\""
        << ": ";
}

void JsonWriter::openObject()
{
    insertComma();
    *os << "{";
    newLine();
    elementCounts.top()++;
    elementCounts.push(0);
}

void JsonWriter::openObject(std::string key)
{
    insertComma();
    addName(key);
    *os << "{";
    newLine();
    elementCounts.top()++;
    elementCounts.push(0);
}

void JsonWriter::openArray()
{
    insertComma();
    *os << "[";
    newLine();
    elementCounts.top()++;
    elementCounts.push(0);
}

void JsonWriter::openArray(std::string key)
{
    insertComma();
    addName(key);
    *os << "[";
    newLine();
    elementCounts.top()++;
    elementCounts.push(0);
}

void JsonWriter::closeObject()
{
    elementCounts.pop();
    *os << std::endl;
    indent();
    *os << "}";
}

void JsonWriter::closeArray()
{
    elementCounts.pop();
    *os << std::endl;
    indent();
    *os << "]";
}

void JsonWriter::addKeyValue(std::string key, int value)
{
    insertComma();
    addName(key);
    *os << value;
    elementCounts.top()++;
}

void JsonWriter::addKeyValue(std::string key, unsigned int value)
{
    insertComma();
    addName(key);
    *os << value;
    elementCounts.top()++;
}

void JsonWriter::addKeyValue(std::string key, long value)
{
    insertComma();
    addName(key);
    *os << value;
    elementCounts.top()++;
}

void JsonWriter::addKeyValue(std::string key, size_t value)
{
    insertComma();
    addName(key);
    *os << value;
    elementCounts.top()++;
}

void JsonWriter::addKeyValue(std::string key, std::string value)
{
    insertComma();
    addName(key);
    *os << "\"" << value << "\"";
    elementCounts.top()++;
}
