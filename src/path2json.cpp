//*************************************************************************************************************
//
//  2016-2017 Regents of the University of California on behalf of the University of California at Berkeley
//       with rights granted for USDOT OSADP distribution with the ECL-2.0 open source license.
//
//*************************************************************************************************************
//
// path2json.cpp
//
// Usage:
//
//     $ path2json -s nats://user:password@host:port
//
#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <nats/nats.h>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

#include "AsnJ2735Lib.h"
#include "JasonWriter.h"
#include "NatsPubSub.h"

void do_usage(const char* progname)
{
    std::cerr << "Usage" << progname << std::endl;
    std::cerr << "\t-u username" << std::endl;
    std::cerr << "\t-p password" << std::endl;
    std::cerr << "\t-h print this message" << std::endl;
    std::cerr << "\t-? print this message" << std::endl;
    exit(EXIT_FAILURE);
}

void bsm2Json(std::ostream& OS, const BSM_element_t& bsm)
{
    JsonWriter json(OS);
    // clang-format off
    json.openObject();
        json.openObject("BSMcoreData");
            json.addKeyValue("msgCn", static_cast<unsigned int>(bsm.msgCnt));
            json.addKeyValue ("id", bsm.id);
            json.addKeyValue("secMark", bsm.timeStampSec);
            json.addKeyValue("lat", bsm.latitude);
            json.addKeyValue("long", bsm.longitude);
            json.addKeyValue("elev", bsm.elevation);
            json.openObject("PositionalAccuracy");
                json.addKeyValue("semiMajor", static_cast<unsigned int>(bsm.semiMajor));
                json.addKeyValue("semiMinor", static_cast<unsigned int>(bsm.semiMinor));
                json.addKeyValue("orientation", bsm.orientation);
            json.closeObject();
            json.addKeyValue("transmission", static_cast<unsigned int>(bsm.transState));
            json.addKeyValue("speed", bsm.speed);
            json.addKeyValue("heading", bsm.heading);
            json.addKeyValue("SteeringWheelAngle", static_cast<int>(bsm.steeringAngle));
            json.openObject("AccelerationSet4Way");
                json.addKeyValue("accLong", bsm.accelLon);
                json.addKeyValue("accLat", bsm.accelLat);
                json.addKeyValue("accVert", static_cast<int>(bsm.accelVert));
                json.addKeyValue("yaw", bsm.yawRate);
            json.closeObject();
            json.openObject("BrakeSystemStatus");
                json.addKeyValue("wheelBrakes", bsm.brakeAppliedStatus.to_string());
                json.addKeyValue("traction", static_cast<unsigned int>(bsm.tractionControlStatus));
                json.addKeyValue("abs", static_cast<unsigned int>(bsm.absStatus));
                json.addKeyValue("scs", static_cast<unsigned int>(bsm.stabilityControlStatus));
                json.addKeyValue("brakeBoost", static_cast<unsigned int>(bsm.brakeBoostApplied));
                json.addKeyValue("auxBrakes:", static_cast<unsigned int>(bsm.auxiliaryBrakeStatus));
            json.closeObject();
            json.openObject("VehicleSize");
                json.addKeyValue("width", bsm.vehWidth);
                json.addKeyValue("length", bsm.vehLen);
            json.closeObject();
        json.closeObject();
    json.closeObject();
    // clang-format on
}

void srm2Json(std::ostream& OS, const SRM_element_t& srm)
{
    JsonWriter json(OS);
    // clang-format off
    json.openObject();
        json.openObject("srm");
            json.addKeyValue("minuteOfYear", srm.timeStampMinute);
            json.addKeyValue("msOfMinute", srm.timeStampSec);
            json.addKeyValue("sequenceNumber", static_cast<unsigned int>(srm.msgCnt));
            json.openArray("SignalRequestList");
                json.openObject();
                    json.openObject("request");
                        json.openObject("id");
                            json.addKeyValue("regionalId", srm.regionalId);
                            json.addKeyValue("intersectionId", srm.intId);
                        json.closeObject();
                        json.addKeyValue("requestID", static_cast<unsigned int>(srm.reqId));
                        json.addKeyValue("requestType", static_cast<unsigned int>(srm.reqType));
                        if (srm.inApprochId != 0)
                                json.addKeyValue("inBoundLane(approach)", static_cast<unsigned int>(srm.inApprochId));
                        else
                                json.addKeyValue("inBoundLane(lane)", static_cast<unsigned int>(srm.inLaneId));
                        if (srm.outApproachId != 0)
                                json.addKeyValue("outBoundLane(approach)", static_cast<unsigned int>(srm.outApproachId));
                        else
                                json.addKeyValue("outBoundLane(lane)",static_cast<unsigned int>(srm.outLaneId));
                        json.addKeyValue("ETAminute", srm.ETAminute);
                        json.addKeyValue("ETAsec", srm.ETAsec);
                        json.addKeyValue("duration", srm.duration);
                    json.closeObject();
                    json.openObject("requestor");
                        json.addKeyValue("VehicleID", srm.vehId);
                        json.openObject("RequestorType");
                            json.addKeyValue("BasicVehicleRole", static_cast<unsigned int>(srm.vehRole));
                            json.addKeyValue("VehicleType:", static_cast<unsigned int>(srm.vehType));
                        json.closeObject();
                        json.openObject("position");
                            json.openObject("Position3D");
                                json.addKeyValue("lat", srm.latitude);
                                json.addKeyValue("long", srm.longitude);
                                json.addKeyValue("elevation", srm.elevation);
                            json.closeObject();
                            json.addKeyValue("heading", srm.heading);
                            json.openObject("TransmissionAndSpeed");
                                json.addKeyValue("transmission", static_cast<unsigned int>(srm.transState));
                                json.addKeyValue("speed", static_cast<unsigned int>(srm.speed));
                            json.closeObject();
                        json.closeObject();
                    json.closeObject();
                json.closeObject();
            json.closeArray();
        json.closeObject();
    json.closeObject();
    // clang-format on
}

void spat2Json(std::ostream& OS, const SPAT_element_t& spat)
{
    JsonWriter json(OS);
    // clang-format off
    json.openObject();
        json.openObject("spat");
            json.openArray("intersections");
                json.openObject();
                    json.openObject("IntersectionState");
                        json.openObject("id");
                            json.addKeyValue("regionalId", spat.regionalId);
                            json.addKeyValue("intersectionId", spat.id);
                        json.closeObject();
                    json.closeObject();
                    json.addKeyValue("msgCnt", static_cast<unsigned int>(spat.msgCnt));
                    json.addKeyValue("minuteOfYear", spat.timeStampMinute);
                    json.addKeyValue("msOfMinute", spat.timeStampSec);
                    json.addKeyValue("status", spat.status.to_string());
                    json.openArray("phaseState");
                        for (int i = 0; i < 8; i++)
                        {
                            if (spat.permittedPhases.test(i))
                            {
                                json.openObject();
                                    const auto& phaseState = spat.phaseState[i];
                                    json.addKeyValue("phase", (i + 1));
                                    json.addKeyValue("currState", static_cast<unsigned int>(phaseState.currState));
                                    json.addKeyValue("startTime", phaseState.startTime);
                                    json.addKeyValue("minEndTime", phaseState.minEndTime);
                                    json.addKeyValue("maxEndTime", phaseState.maxEndTime);
                                json.closeObject();
                            }
                        }
                    json.closeArray();
                    json.openArray("pedPhaseState");
                        for (int i = 0; i < 8; i++)
                        {
                            if (spat.permittedPedPhases.test(i))
                            {
                                json.openObject();
                                    const auto& phaseState = spat.pedPhaseState[i];
                                    json.addKeyValue("phase", (i + 1));
                                    json.addKeyValue("currState", static_cast<unsigned int>(phaseState.currState));
                                    json.addKeyValue("startTime", phaseState.startTime);
                                    json.addKeyValue("minEndTime", phaseState.minEndTime);
                                    json.addKeyValue("maxEndTime", phaseState.maxEndTime);
                                json.closeObject();
                            }
                        }
                    json.closeArray();
                json.closeObject();
            json.closeArray();
        json.closeObject();
    json.closeObject();
    // clang-format on
}

void ssm2Json(std::ostream& OS, const SSM_element_t& ssm)
{
    // clang-format off
    JsonWriter json(OS);
    json.openObject();
        json.openObject("ssm");
            json.addKeyValue("minuteOfYear", ssm.timeStampMinute);
            json.addKeyValue("msOfMinute", ssm.timeStampSec);
            json.addKeyValue("sequenceNumber:", static_cast<unsigned int>(ssm.msgCnt));

            json.openArray("SignalStatusList");
                json.openObject();
                    json.addKeyValue("updateCnt" ,static_cast<unsigned int>(ssm.updateCnt));
                    json.openObject("id");
                        json.addKeyValue("regionalId", ssm.regionalId);
                        json.addKeyValue("intersectionId", ssm.id);
                    json.closeObject();
                    json.openArray("SignalRequest");
                        size_t reqNo = 1;
                        for (const auto& RequetStatus : ssm.mpSignalRequetStatus)
                        {
                            json.openObject();
                                json.addKeyValue("request#", reqNo++);
                                json.openObject("requester");
                                    json.addKeyValue("VehicleID", RequetStatus.vehId);
                                    json.addKeyValue("RequestID", static_cast<unsigned int>(RequetStatus.reqId));
                                    json.addKeyValue("sequenceNumber", static_cast<unsigned int>(RequetStatus.sequenceNumber));
                                    json.addKeyValue("role", static_cast<unsigned int>(RequetStatus.vehRole));
                                json.closeObject();                    
                                if (RequetStatus.inApprochId != 0)
                                {
                                    json.addKeyValue("inBoundLane(approach)", static_cast<unsigned int>(RequetStatus.inApprochId));
                                }
                                else
                                {
                                    json.addKeyValue("inBoundLane(lane)", static_cast<unsigned int>(RequetStatus.inLaneId));
                                }
                                if (RequetStatus.outApproachId != 0)
                                {
                                    json.addKeyValue("outBoundLane(approach)", static_cast<unsigned int>(RequetStatus.outApproachId));
                                }
                                else
                                {
                                    json.addKeyValue("outBoundLane(lane)", static_cast<unsigned int>(RequetStatus.outLaneId));
                                }
                                json.addKeyValue("ETAminute", RequetStatus.ETAminute);
                                json.addKeyValue("ETAsec", RequetStatus.ETAsec);
                                json.addKeyValue("duration", RequetStatus.duration);
                                json.addKeyValue("PRS status", static_cast<unsigned int>(RequetStatus.status));
                            json.closeObject();
                        }
                    json.closeArray();
                json.closeObject();
            json.closeArray();
        json.closeObject();
    json.closeObject();
    // clang-format on
}

void uper2json(std::vector<uint8_t> buf, std::stringstream& s, std::string& msgType)
{
    Frame_element_t dsrcFrame;
    if (AsnJ2735Lib::decode_msgFrame(buf.data(), buf.size(), dsrcFrame) > 0)
    {
        switch (dsrcFrame.dsrcMsgId)
        {
            case MsgEnum::DSRCmsgID_bsm:
            {
                BSM_element_t& bsm = dsrcFrame.bsm;
                bsm2Json(s, bsm);
                msgType = "bsm";
            }
            break;
            case MsgEnum::DSRCmsgID_srm:
            {
                SRM_element_t& srm = dsrcFrame.srm;
                srm2Json(s, srm);
                msgType = "srm";
            }
            break;
            case MsgEnum::DSRCmsgID_spat:
            {
                SPAT_element_t& spat = dsrcFrame.spat;
                spat2Json(s, spat);
                msgType = "spat";
            }
            break;
            case MsgEnum::DSRCmsgID_ssm:
            {
                SSM_element_t& ssm = dsrcFrame.ssm;
                ssm2Json(s, ssm);
                msgType = "ssm";
            }
            break;
            default:
            {
                std::cerr << "ERROR: Unkown message ID received" << std::endl;
                msgType = "";
            }
            break;
        }
    }
    else
    {
        std::cerr << "ERROR: Failed decode_msgFrame" << std::endl;
    }
}

NatsPubSub* ps;

void onMsg(std::string subject, std::vector<uint8_t> payLoad)
{
    std::string msgType;
    std::stringstream jsonStream;
    uper2json(payLoad, jsonStream, msgType);

    // Publish to NATS
    std::string json = jsonStream.str();
    std::vector<uint8_t> payload = std::vector<uint8_t>(json.c_str(), json.c_str() + json.length());
    std::string jsonSubject = "path." + msgType + ".json";
    ps->publish(jsonSubject, payload);
}

int main(int argc, char** argv)
{
    std::string natsUrl;

    std::string user;
    std::string password;

    int option;
    while ((option = getopt(argc, argv, "u:p:h:?")) != EOF)
    {
        switch (option)
        {
            case 'u':
                user = optarg;
                break;
            case 'p':
                password = optarg;
                break;
            case '?':
            case 'h':
            default:
                do_usage(argv[0]);
                break;
        }
    }

    if ((user.size() == 0) || (password.size() == 0))
    {
        do_usage(argv[0]);
    }

    const char* servers[] = {
        "nats://nats-1.rsm-technologies.com:4222",
        "nats://nats-2.rsm-technologies.com:4222",
        "nats://nats-3.rsm-technologies.com:4222"
    };
    int serversCount = sizeof(servers) / sizeof(servers[0]);

    std::string cacert = "../certs/ca/ca-pub-cert.pem";
    std::string certChain = "../certs/client/client-cert-chain.pem";
    std::string privateKey = "../certs/client/client-priv-key.pem";

    NatsPubSub pubsub(servers, serversCount, cacert, certChain, privateKey, user, password);
    ps = &pubsub;

    std::string subject = ("path.*.uper");
    pubsub.subscribe(subject, onMsg);

    // Wait for it...
    while (1)
    {
        nats_Sleep(100);
    }

    std::cerr << "INFO: path2json exited" << std::endl;

    return (0);
}
