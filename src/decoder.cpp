#include <fstream>
#include <iostream>
#include <sstream>
#include <sys/types.h>
#include <vector>

#include "JasonWriter.h"
#include "SensorMonitor.h" // SensorMonitor ASN.1 type

void printHex(std::vector<uint8_t>& buf)
{
    // Save default formatting
    std::ios init(NULL);
    init.copyfmt(std::cout);

    for (auto i : buf)
    {
        std::cout.width(2);
        std::cout.fill('0');
        std::cout << std::hex << (int)i << " ";
    }
    std::cout << std::endl;

    // Restore default formatting
    std::cout.copyfmt(init);
}

void printHex(const uint8_t* buf, size_t length)
{
    auto b = std::vector<uint8_t>(buf, buf + length);
    printHex(b);
}

void printHex(const char* buf, size_t length)
{
    auto b = std::vector<uint8_t>(buf, buf + length);
    printHex(b);
}

void sensorMonitor2Json(std::ostream& OS, const SensorMonitor_t& sensorMonitor)
{
    JsonWriter json(OS);
    json.openObject();
    json.openObject("SensorMonitor");
    json.addKeyValue("timestamp", (char*)sensorMonitor.sensorMonitor.timestamp.buf);
    json.openArray("stats");
    for (int i = 0; i < sensorMonitor.sensorMonitor.stats.list.count; i++)
    {
        json.openObject();
        json.addKeyValue("sourceId", (unsigned int)sensorMonitor.sensorMonitor.stats.list.array[i]->sourceId);
        json.addKeyValue("type", (char*)sensorMonitor.sensorMonitor.stats.list.array[i]->type.buf);
        json.addKeyValue("avgDataRate", sensorMonitor.sensorMonitor.stats.list.array[i]->avgDataRate);
        json.addKeyValue("avgLatency", sensorMonitor.sensorMonitor.stats.list.array[i]->avgLatency);
        json.addKeyValue("maxLatency", sensorMonitor.sensorMonitor.stats.list.array[i]->maxLatency);
        json.addKeyValue("errorCount", sensorMonitor.sensorMonitor.stats.list.array[i]->errorCount);
        json.closeObject();
    }
    json.closeArray();
    json.closeObject();
    json.closeObject();
}

int main(int argc, char** argv)
{
    SensorMonitor_t* sensorMonitor = 0; // Type to decode. Note this 0!
    char* fileName; // Input file name

    // Require a single filename argument
    if (argc != 2)
    {
        std::cout << "Usage: " << argv[0] << " <file.per>" << std::endl;
        exit(1);
    }
    else
    {
        fileName = argv[1];
    }

    // Read the file
    std::ifstream inStream(fileName, std::ios_base::binary);
    std::stringstream s;
    s << inStream.rdbuf();
    inStream.close();
    std::string input = s.str();
    printHex(input.data(), input.size());

    // Decode the input buffer as SensorMonitor type
    asn_codec_ctx_t opt_codec_ctx;
    opt_codec_ctx.max_stack_size = 0;
    auto res = asn_decode(&opt_codec_ctx, ATS_UNALIGNED_BASIC_PER, &asn_DEF_SensorMonitor, (void**)&sensorMonitor, input.data(), input.size());
    if (res.code != RC_OK)
    {
        std::cout << fileName << ": Broken SensorMonitor encoding at byte " << res.consumed << std::endl;
        exit(1);
    }

    sensorMonitor2Json(std::cout, *sensorMonitor);

    // Print the decoded SensorMonitor type as XML
    //    xer_fprint(stdout, &asn_DEF_SensorMonitor, sensorMonitor);

    return 0; // Decoding finished successfully
}
