#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdio.h>
#include <string>
#include <sys/types.h>
#include <vector>

#include <SensorMonitor.h> // SensorMonitor ASN.1 type

void printHex(std::vector<uint8_t>& buf)
{
    for (auto i : buf)
    {
        std::cout.width(2);
        std::cout.fill('0');
        std::cout << std::hex << (int)i << " ";
    }
    std::cout << std::endl;
}

int main(int argc, char** argv)
{
    // Allocate the SensorMonitor_t
    auto sensorMonitor = (SensorMonitor_t*)calloc(1, sizeof(SensorMonitor_t)); // not malloc!
    if (!sensorMonitor)
    {
        perror("calloc() failed");
        exit(1);
    }

    // Fill in the timestamp
    sensorMonitor->sensorMonitor.timestamp.buf = (uint8_t*)"Moday";
    sensorMonitor->sensorMonitor.timestamp.size = 5;

    // Fill in the stats list
    for (int i = 0; i < 8; i++)
    {
        // Create a new stats item
        Member* member = (Member*)calloc(1, sizeof(Member)); // not malloc!
        if (!member)
        {
            perror("Member calloc() failed");
            exit(1);
        }

        // Fill in the stats details
        member->sourceId = 6;
        const char* type = "Connected Signals";
        member->type.buf = (uint8_t*)type;
        member->type.size = strlen(type);
        member->avgDataRate = 2500;
        member->avgLatency = 10;
        member->maxLatency = 200;
        member->errorCount = 8982;

        // Add the item to the stats list
        asn_sequence_add(&sensorMonitor->sensorMonitor.stats.list, member);
    }

    // Print the constructed sensorMonitor message XER encoded (XML)
    xer_fprint(stdout, &asn_DEF_SensorMonitor, sensorMonitor);
    //    asn_fprint(stdout, &asn_DEF_SensorMonitor, sensorMonitor);

    asn_codec_ctx_t opt_codec_ctx;
    opt_codec_ctx.max_stack_size = 0;

    auto enc = asn_encode_to_new_buffer(&opt_codec_ctx, ATS_UNALIGNED_BASIC_PER, &asn_DEF_SensorMonitor, sensorMonitor);
    printf("Result length = %d\n", enc.result.encoded);
    if (enc.result.encoded == -1)
    {
        fprintf(stderr, "Could not encode SensorMonitor (at  asn_codec_ctx_t%s)\n",
            enc.result.failed_type ? enc.result.failed_type->name : "unknown");
        exit(1);
    }
    else
    {
        fprintf(stderr, "Created buff with UPER encoded SensorMonitor\n");
        std::vector<uint8_t> b = std::vector<uint8_t>((uint8_t*)enc.buffer, (uint8_t*)enc.buffer + enc.result.encoded);
        printHex(b);
    }

    // Write out encoded the data if filename is given
    if (argc < 2)
    {
        fprintf(stderr, "Specify filename for output\n");
    }
    else
    {
        std::ofstream writeFile;
        writeFile.open(argv[1], std::ios::out | std::ios::binary);
        if (enc.result.encoded > 0)
        {
            writeFile.write((char*)enc.buffer, enc.result.encoded);
        }
    }
    return 0; // Encoding finished successfully
}
