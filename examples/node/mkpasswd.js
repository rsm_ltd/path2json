const bcrypt = require('bcrypt')
const PasswordGenerator = require('strict-password-generator').default;

const passwordGenerator = new PasswordGenerator();


const passwdOptions = {
  upperCaseAlpha   : false,
  lowerCaseAlpha   : true,
  number           : true,
  specialCharacter : false,
  minimumLength    : 12,
  maximumLength    : 14
}

const password = passwordGenerator.generatePassword(passwdOptions);

const saltRounds = 11

var salt = bcrypt.genSaltSync(saltRounds, 'a')

var hash = bcrypt.hashSync(password, salt)

console.log('pass: ', password)
console.log('bcrypt hash:', hash);








