

let nats = require('nats')
let fs = require('fs');

let argv = require('minimist')(process.argv.slice(2))

let servers = [
  "nats://nats-1.rsm-technologies.com:4222",
  "nats://nats-2.rsm-technologies.com:4222",
  "nats://nats-3.rsm-technologies.com:4222"
]

let tlsOptions = {
  key:  fs.readFileSync('./certs/client/client-priv-key.pem'),
  cert: fs.readFileSync('./certs/client/client-pub-cert.pem'),
  ca: [ fs.readFileSync('./certs/ca/ca-pub-cert.pem') ],
  rejectUnauthorized: false
}

let nc = nats.connect({
  servers: servers,
  user: argv.u,
  pass: argv.p,
  tls: tlsOptions,
  maxReconnectAttempts: -1,
  reconnectTimeWait: 1000
})

nc.subscribe('path.*.json', function(msg, reply, subject) {
  let obj = JSON.parse(msg)
  console.log(JSON.stringify(obj, null, '    '))
})

nc.on('error', function(err) {
  console.log(err)
})

nc.on('connect', function(nc) {
  console.log('connected: ', nc.stream.authorizationError)
  console.log('Authorized = ', nc.stream.authorized)
})

nc.on('disconnect', function() {
  console.log('disconnect')
})

nc.on('reconnecting', function() {
  console.log('reconnecting')
})

nc.on('reconnect', function(nc) {
  console.log('reconnect')
})

nc.on('close', function() {
  console.log('close')
});

