//*************************************************************************************************************
//
// Â© 2016-2017 Regents of the University of California on behalf of the University of California at Berkeley
//       with rights granted for USDOT OSADP distribution with the ECL-2.0 open source license.
//
//*************************************************************************************************************
//
// path-nats.cpp
//
// Usage: path-nats -s nats://user:password@host:port
//

#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <nats/nats.h>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

#include "AsnJ2735Lib.h"
#include "JasonWriter.h"
#include "NatsPubSub.h"

void do_usage(const char* progname)
{
    std::cerr << "Usage" << progname << std::endl;
    std::cerr << "\t-u username" << std::endl;
    std::cerr << "\t-p password" << std::endl;
    std::cerr << "\t-? print this message" << std::endl;
    std::cerr << "\t-h print this message" << std::endl;
    exit(EXIT_FAILURE);
}

// Parameters
const uint16_t regionalId = 0;
const uint16_t intersectionId = 1003; //    page-mill
const uint32_t vehId = 601;
const uint8_t priorityLevel = 5;
const uint8_t inLaneId = 8; //    page-mill, northbound ECR, inbound middle through lane
const uint8_t outLaneId = 30; //    page-mill, northbound ECR, outbound middle through lane
const uint16_t heading = (uint16_t)(std::round(60.0 / 0.0125)); //    60.0 degree
const uint16_t speed = (uint16_t)(std::round(35 * 0.277778 / 0.02)); //    35 KPH
int32_t latitude = 374230638;
int32_t longitude = -1221420467;
int32_t elevation = 126;
const uint32_t time2go = 30000; //    30 seconds
const uint16_t duration = 2000; //    2 seconds
const uint32_t minuteOfYear = 56789;
const uint16_t msOfMinute = 12345;
uint32_t ETAsec = time2go;

void fillBSM(Frame_element_t* dsrcFrame)
{
    // Manual input bsm
    dsrcFrame->dsrcMsgId = MsgEnum::DSRCmsgID_bsm;
    BSM_element_t& bsm = dsrcFrame->bsm;
    bsm.reset();
    bsm.msgCnt = 1;
    bsm.id = vehId;
    bsm.timeStampSec = msOfMinute;
    bsm.latitude = latitude;
    bsm.longitude = longitude;
    bsm.elevation = elevation;
    bsm.yawRate = 0;
    bsm.vehLen = 1200;
    bsm.vehWidth = 300;
    bsm.speed = speed;
    bsm.heading = heading;
}

void fillSRM(Frame_element_t* dsrcFrame)
{
    // Manual input srm
    dsrcFrame->dsrcMsgId = MsgEnum::DSRCmsgID_srm;
    SRM_element_t& srm = dsrcFrame->srm;
    srm.reset();
    srm.timeStampMinute = minuteOfYear;
    srm.timeStampSec = msOfMinute;
    srm.msgCnt = 2;
    srm.regionalId = regionalId;
    srm.intId = intersectionId;
    srm.reqId = priorityLevel;
    srm.inApprochId = 0;
    srm.inLaneId = inLaneId;
    srm.outApproachId = 0;
    srm.outLaneId = outLaneId;
    srm.ETAsec = static_cast<uint16_t>((ETAsec % 60000) & 0xFFFF);
    srm.ETAminute = static_cast<uint32_t>(minuteOfYear + std::floor(ETAsec / 60000));
    srm.duration = duration;
    srm.vehId = vehId;
    srm.latitude = latitude;
    srm.longitude = longitude;
    srm.elevation = elevation;
    srm.heading = heading;
    srm.speed = speed;
    srm.reqType = MsgEnum::requestType::priorityRequest;
    srm.vehRole = MsgEnum::basicRole::transit;
    srm.vehType = MsgEnum::vehicleType::bus;
}

void fillSPaT(Frame_element_t* dsrcFrame)
{
    // Manual input spat
    dsrcFrame->dsrcMsgId = MsgEnum::DSRCmsgID_spat;
    SPAT_element_t& spat = dsrcFrame->spat;
    spat.reset();
    spat.regionalId = regionalId;
    spat.id = intersectionId;
    spat.msgCnt = 3;
    spat.timeStampMinute = minuteOfYear;
    spat.timeStampSec = msOfMinute;
    spat.permittedPhases.set(); // All 8 phases permitted
    for (size_t i = 0; i < 8; i++)
    {
        if (i % 2 == 1)
            spat.permittedPedPhases.set(i); // Even phase # is pedestrian phase
    }
    spat.status.reset();
    uint16_t currentTime = static_cast<uint16_t>((minuteOfYear % 60) * 600 + msOfMinute / 100);
    uint16_t baseTime = 50;
    uint16_t stepTime = 50;
    //    std::cout << "tenths of a second in the current hour: " << currentTime << std::endl;
    for (auto& phaseState : spat.phaseState)
    {
        phaseState.currState = MsgEnum::phaseState::redLight;
        phaseState.startTime = baseTime;
        phaseState.minEndTime = (uint16_t)(phaseState.startTime + stepTime);
        phaseState.maxEndTime = (uint16_t)(phaseState.minEndTime + stepTime);
        baseTime = (uint16_t)(phaseState.minEndTime + stepTime);
    }
    spat.phaseState[1].currState = MsgEnum::phaseState::protectedGreen;
    spat.phaseState[5].currState = MsgEnum::phaseState::protectedGreen;
    for (size_t i = 0; i < 8; i++)
    {
        if (i % 2 == 0)
            continue;
        auto& phaseState = spat.pedPhaseState[i];
        phaseState.currState = MsgEnum::phaseState::redLight;
        phaseState.startTime = baseTime;
        phaseState.minEndTime = (uint16_t)(phaseState.startTime + stepTime);
        phaseState.maxEndTime = (uint16_t)(phaseState.minEndTime + stepTime);
        baseTime = (uint16_t)(phaseState.minEndTime + stepTime);
    }
    spat.pedPhaseState[1].currState = MsgEnum::phaseState::protectedYellow;
    spat.pedPhaseState[5].currState = MsgEnum::phaseState::protectedYellow;
}

void fillSSM(Frame_element_t* dsrcFrame)
{
    // Manual input ssm
    dsrcFrame->dsrcMsgId = MsgEnum::DSRCmsgID_ssm;
    SSM_element_t& ssm = dsrcFrame->ssm;
    ssm.reset();
    ssm.timeStampMinute = minuteOfYear;
    ssm.timeStampSec = msOfMinute;
    ssm.msgCnt = 4;
    ssm.updateCnt = 1;
    ssm.regionalId = regionalId;
    ssm.id = intersectionId;
    SignalRequetStatus_t requestStatus;
    requestStatus.reset();
    requestStatus.vehId = vehId;
    requestStatus.reqId = priorityLevel;
    requestStatus.sequenceNumber = 2;
    requestStatus.vehRole = MsgEnum::basicRole::transit;
    requestStatus.inLaneId = inLaneId;
    requestStatus.outLaneId = outLaneId;
    requestStatus.ETAminute = static_cast<uint32_t>(minuteOfYear + std::floor(ETAsec / 60000));
    requestStatus.ETAsec = static_cast<uint16_t>((ETAsec % 60000) & 0xFFFF);
    requestStatus.duration = duration;
    requestStatus.status = MsgEnum::requestStatus::granted;
    ssm.mpSignalRequetStatus.push_back(requestStatus);
}

void sendDsrcFrame(NatsPubSub* pubsub, std::string subject, Frame_element_t* dsrcFrame)
{
    // Buffer to hold message payload
    size_t bufSize = 2000;
    std::vector<uint8_t> buf(bufSize, 0);

    // Encode BSM payload
    size_t payload_size = AsnJ2735Lib::encode_msgFrame(*dsrcFrame, &buf[0], bufSize);
    if (payload_size <= 0)
    {
        std::cerr << "ERROR: Encode payload failed" << std::endl;
    }

    pubsub->publish(subject, buf);
}

int main(int argc, char** argv)
{
    std::string natsUrl;
    std::string user;
    std::string password;

    int option;
    while ((option = getopt(argc, argv, "u:p:?")) != EOF)
    {
        switch (option)
        {
            case 'u':
                user = optarg;
                break;
            case 'p':
                password = optarg;
                break;
            case '?':
            case 'h':
            default:
                do_usage(argv[0]);
                break;
        }
    }

    if ((user.size() == 0) || (password.size() == 0))
    {
        do_usage(argv[0]);
    }

    const char* servers[] = {
        "nats://nats-1.rsm-technologies.com:4222",
        "nats://nats-2.rsm-technologies.com:4222",
        "nats://nats-3.rsm-technologies.com:4222"
    };
    int serversCount = sizeof(servers) / sizeof(servers[0]);

    std::string subject = "test.5yeLABoxYt0o";
    std::string cacert = "../certs/ca/ca-pub-cert.pem";
    std::string certChain = "../certs/client/client-cert-chain.pem";
    std::string privateKey = "../certs/client/client-priv-key.pem";
    NatsPubSub pubsub(servers, serversCount, cacert, certChain, privateKey, user, password);

    // Input to UPER encoding function
    Frame_element_t dsrcFrame;

    while (1)
    {
        dsrcFrame.reset();

        fillBSM(&dsrcFrame);
        sendDsrcFrame(&pubsub, "path.ssm.uper", &dsrcFrame);
        nats_Sleep(100);

        fillSRM(&dsrcFrame);
        sendDsrcFrame(&pubsub, "path.ssm.uper", &dsrcFrame);
        nats_Sleep(100);

        fillSPaT(&dsrcFrame);
        sendDsrcFrame(&pubsub, "path.ssm.uper", &dsrcFrame);
        nats_Sleep(100);

        fillSSM(&dsrcFrame);
        sendDsrcFrame(&pubsub, "path.ssm.uper", &dsrcFrame);
        nats_Sleep(100);
    }

    std::cout << "Done test encode and publish" << std::endl;
    return (0);
}
