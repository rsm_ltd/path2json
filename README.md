# path2json

## Installation

### Install Dependencies

Ensure cmake, g++ and openssl dev headers are available:
```
    $ apt-get install cmake
    $ apt-get install g++
    $ apt-get install libssl-dev
```

Install the cnats NATS client library for C:
```
    $ git clone https://github.com/nats-io/cnats.git
    $ cd cnats 
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ sudo make install
```

The NATS libraries and includes will be installed to /usr/local.

Install path_dsrc sources and build the AsnJ2735Lib
```
    $ cd ~
    $ git clone https://zicog@bitbucket.org/rsm_ltd/path_dsrc.git
    $ cd path_dsrc
    $ make
```

### Build

Build path2json and the path-nats UPER message test generator:
```
    $ cd ~
    $ git clone https://zicog@bitbucket.org/rsm_ltd/path2json.git
    $ cd path2json
    $ mkdir build
    $ cd build
    $ cmake ..
```

## Test

The path2json process is running on the RSM cloud servers at:

* nats-1.rsmtechnologies.com
* nats-2.rsmtechnologies.com
* nats-3.rsmtechnologies.com

To send example DSRC messages to the NATS cluster run the path-nats DSRC UPER test message publisher:
```
    $ export LD_LIBRARY_PATH=/usr/local/lib
    $ ./path-nats -u <userName> -p <password>
```
This should result in the following output, with no error messages:
```
    INFO: NATS Connected
    INFO: Max payload allowed = 1024KB
```

Inorder to verify end to end operation of DSRC message communication the DSRC messages can be recieved 
back again, in JSON format by running pathjason the node.js script

In another terminal receive messages back from the NATS server and display in JSON format:
```
    $ cd <path2json directory>
    node ./examples/node/pathjason.js -u <userName> -p <password>
```

## WARNING

The certificates and keys in this repository are for testing only. Do not use in production!




