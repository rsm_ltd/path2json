# Systemd Service

/etc/systemd/system/nats.service
```
    [Unit]
    Description=NATS messaging server

    [Service]
    ExecStart=/home/rsm/nats/bin/gnatsd -c /home/rsm/nats/gnatsd.config
    User=nats
    Restart=on-failure
```
## TLS

Use the following command to create a self-Signed certificate
```
    $ mkdir ~/priv`
    $ openssl req -x509 -nodes -days 3650 -newkey rsa:2048 \
        -keyout priv/gnatsd.key -out priv/gnatsd.crt \
        -subj "/C=FI/ST=Uusimaa/L=Helsinki/O=RSM/CN=rsm.com"
```

Install the cert and key and set permissions:
```
    $ sudo mv ~/priv /home/rsm/nats

    Now, make /home/rsm/nats/priv accessible to only to the rsm user and group:

    $ sudo chmod 440 /home/rsm/nats/priv/*
    $ sudo chmod 550 /home/rsm/nats/priv
    $ sudo chown -R rsm:rsm /home/rsm/nats/priv
```

## The gnatsd.config file
```
    tls {
      cert_file: "/home/rsm/nats/priv/gnatsd.crt"
      key_file: "/home/rsm/nats/priv/gnatsd.key"
      timeout: 1
    }

    authorization {

      ADMIN = {
        publish = ">"
        subscribe = ">"
      }

      PATH = {
        publish = ["path.*.uper"]
        subscribe = ["path.*.uper"]
      }

      DB = {
        publish = []
        subscribe = ["path.json"]
      }

      DEFAULT_PERMISSIONS = {
        publish = "SANDBOX.*"
        subscribe = ["PUBLIC.>", "_INBOX.>"]
      }

      PASS: baz

      users = [
        {user: rsm,  password: foo,   permissions: $ADMIN}
        {user: path, password: bar,   permissions: $PATH}
        {user: db,   password: $PASS, permissions: $DB}
      ]
    }
```

## References

- https://nats.io/documentation/server/gnatsd-authorization/
- https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-nats-on-ubuntu-16-04

