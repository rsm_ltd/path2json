#!/bin/bash
#
# Make self signed server and client certificates.
#
# Based on these Docker instructions:  https://docs.docker.com/engine/security/https/#other-modes
#
# See also : http://wiki.cacert.org/FAQ/subjectAltName
#
# CREATE A CERTIFICATE AUTHORITY
#
# First we need a private key for the CA
# Edit a  passphrase in the file ca/passphrase.txt then:
openssl genrsa -aes256 -passout file:ca/passphrase.txt -out ca/ca-priv-key.pem 4096 

# and we need a public certificate
DN="/C=US/ST=California/L=San Jose/O=RSM Corp./OU=PKI"
DAYS=$((8*365))
HOST=localhost
openssl req -new -x509 -days $DAYS -subj "$DN/CN=$HOST" -key ca/ca-priv-key.pem -sha256 -passin file:ca/passphrase.txt -out ca/ca-pub-cert.pem

echo "---------------------------------------------------------------------------------------------"

# CREATE SERVER KEY AND SIGNED CERT
#
# First we need a private key for the server
openssl genrsa -out server/server-priv-key.pem 4096

# then a certificate signing request
openssl req -subj "/CN=$HOST" -sha256 -new -key server/server-priv-key.pem -out server/server.csr

# then sign the server's public key with the ca:
echo subjectAltName = DNS:$HOST,IP:127.0.0.1 >> extfile.cnf                # What are we known as
echo extendedKeyUsage = serverAuth >> extfile.cnf                          # Use for server authentication only

openssl x509 -req -days $DAYS -sha256 -passin file:ca/passphrase.txt -in server/server.csr -CA ca/ca-pub-cert.pem -CAkey ca/ca-priv-key.pem -CAcreateserial -out server/server-pub-cert.pem -extfile extfile.cnf

echo "---------------------------------------------------------------------------------------------"

# CREATE CLIENT KEY AND SIGNED CERT FOR CLIENT AUTHENTICATION

# First we need a private key for the client
openssl genrsa -out client/client-priv-key.pem 4096

# then a certificate signing request
openssl req -subj '/CN=$CLIENT' -new -key client/client-priv-key.pem -out client/client.csr

echo extendedKeyUsage = clientAuth >> extfile.cnf       # Use for client authentication only

openssl x509 -req -days $DAYS -sha256 -passin file:ca/passphrase.txt -in client/client.csr -CA ca/ca-pub-cert.pem -CAkey ca/ca-priv-key.pem -CAcreateserial -out client/client-pub-cert.pem -extfile extfile.cnf

echo "---------------------------------------------------------------------------------------------"

# NAke the client cert chain required by natsc
cat client/client-pub-cert.pem ca/ca-pub-cert.pem > client/client-cert-chain.pem



