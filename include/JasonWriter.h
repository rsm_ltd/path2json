#include <stack>
#include <iostream>
#include <sstream>
#include <string>

class JsonWriter {
    std::stack <int> elementCounts;
    std::ostream *os;
public:
    JsonWriter (std::ostream& os);
    void openObject();
    void openObject(std::string key);
    void closeObject();
    void openArray();
    void openArray(std::string key);
    void closeArray();
    void addKeyValue(std::string key, int value);
    void addKeyValue(std::string key, unsigned int value);
    void addKeyValue(std::string key, long value);
    void addKeyValue(std::string key, size_t value);
    void addKeyValue(std::string key, std::string value);
private:
    void newLine();
    void indent();
    void addName(std::string name);
    void insertComma();
};
