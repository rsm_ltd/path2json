#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <unistd.h>
#include <iomanip>
#include <functional>

#include <nats/nats.h>

class NatsPubSub {
public:
    NatsPubSub (const char* servers[], int serversCount, const std::string& caCert, const std::string& certChain, const std::string& privateKey, const std::string& user, const std::string& password);
    ~NatsPubSub();

    void publish (std::string subject, std::vector<uint8_t> buf);

    void subscribe (std::string subject, std::function<void (std::string, std::vector<uint8_t>)> callBack);

private:
    natsOptions * opts = NULL;
    natsConnection *conn = NULL;
    natsSubscription *sub  = NULL;
    std::function<void (std::string, std::vector<uint8_t>)> callBack = 0;
};
